# Asi's Gui API

## What is this?

This Gui API is designed to give people a simple yet efficient way to create their own Minecraft Gui's.
It is currently still a work-in-progress, so there may be a few problems found.

### Current Features

#### Main Parts

* Base Minecraft Gui Grabber
    * Takes control of the mouse, pauses singleplayer etc.
    * Handles the base gui render to prevent unwanted depth problems (one render being infront/behind the current gui)
* Custom Gui Manager
    * Handles all custom guis' and components' updates and renders
    * Captures custom mouse and keyboard input to ensure no keystrokes/mouse presses are missed
    * Allows functions run on gui closing

#### Components

* Container
    * Contains components (in the "components" list)
    * Allows scrolling if components extend container height (with a defined scroll increment)
    * Allows component offsetting (useful for Gui's that don't just list the buttons one after the other)
    * Allows component clipping (use GL scissor to prevent components extending the size of the container to show
* Button
    * Allows specific function handling on click
* Multichoice Button
    * Allows choice selection (the alternative option to a dropdown list)
* Slider
    * Allows value selection (value, min, max, value increment)
    * Padding available for mouse interaction (padding array (double[]))
* Textbox
    * Padding available for text interaction offsetting
    * Text handling shortcuts
        * `CTRL+A` Select all
        * `CTRL+C` Copy selection
        * `CTRL+X` Cut selection
        * `CTRL+V` Paste clipboard (overrides current selection if text is selected)
        * `CTRL+LEFT` and `CTRL+RIGHT` Move cursor per word rather than character
        * `CTRL+DELETE` Remove the word subsequent of the cursor
        * `CTRL+BACKSPACE` Remove the word prior to the cursor
        * `HOME` Move cursor to beginning of text
        * `END` Move cusror to end of text
        * `SHIFT+Cursor Move` Start text selection
    * Text limits (defaults to 10000)
* Toggle
    * Allows value toggling (true/false values)

#### Font

* Tools
    * Hiero (for font generation)
        * If using Hiero, set X and Y at 0, and padding values to 2 for the font's to work nicely. Will fix this in the future.
* Font Loader
    * Pulls information from "jar::assets/minecraft/clients/fonts/<fontname>/<fontsize>"
    * Files needed/provided by Hiero: *.png, *.fnt
* Font Rendering
    * Handles all Minecraft color and formatting codes (0123456789 abcdef klmnor)
* Fonts Provided
    * FontAwesome
    * Verdana