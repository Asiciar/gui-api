package me.asi.client.value;

public class Value<T> {

    public T[] t;

    @SafeVarargs
    public Value(T... t) {
        this.t = t;
    }

}
