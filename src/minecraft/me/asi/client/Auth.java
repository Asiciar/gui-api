package me.asi.client;

import java.net.Proxy;

import com.mojang.authlib.Agent;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

public class Auth {

	private static Minecraft mc = Minecraft.getMinecraft();

	public static int setSessionData(String user, String pass) {
		if (pass.length() != 0) {
			YggdrasilAuthenticationService authentication = new YggdrasilAuthenticationService(Proxy.NO_PROXY, "");
			YggdrasilUserAuthentication auth = (YggdrasilUserAuthentication) authentication
					.createUserAuthentication(Agent.MINECRAFT);
			auth.setUsername(user);
			auth.setPassword(pass);
			try {
				auth.logIn();
				mc.session = new Session(auth.getSelectedProfile().getName(),
						auth.getSelectedProfile().getId().toString(), auth.getAuthenticatedToken(), "Legacy");
				return 1;
			} catch (Exception ignored) {
			}
			return 0;
		} else {
			mc.session = new Session(user, "", "", "Legacy");
			return 2;
		}
	}	
	
}
