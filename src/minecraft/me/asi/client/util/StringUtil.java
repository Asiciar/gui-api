package me.asi.client.util;

public class StringUtil {

    public static String insert(String base, String add, int index) {
        if (index < 0 || index > base.length()) {
            return base;
        }

        String out = "";
        out+= base.substring(0, index);
        out+= add;
        out+= base.substring(index);
        return out;
    }

    public static String replace(String base, String add, int start, int end) {
        if (start < 0 || start > base.length() || end < 0 || end > base.length()) {
            return base;
        }

        String out = "";
        out+= base.substring(0, Math.min(start, end));
        out+= add;
        out+= base.substring(Math.max(start, end));
        return out;
    }

    public static String remove(String base, int from, int to) {
        int temp = from;
        if (from > to) {
            from = to;
            to = temp;
        }

        if (from < 0 || from > base.length() || to < 0 || to > base.length()) {
            return base;
        }

        String out = "";
        out+= base.substring(0, from);
        out+= base.substring(to);
        return out;
    }

}
