package me.asi.client.util;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import me.asi.client.gui.GuiGrabber;
import me.asi.client.gui.GuiManager;
import me.asi.client.gui.test.GuiTest;
import net.minecraft.client.Minecraft;

public class Input {

	private static Minecraft mc = Minecraft.getMinecraft();
	
	public static int mx() {
		return Mouse.getX();
	}
	
	public static int my() {
		return Display.getHeight() - Mouse.getY();
	}
	
	public static void keyboard() {
		if (Keyboard.getEventKeyState() && Keyboard.getEventKey() == Keyboard.KEY_RSHIFT) {
			mc.displayGuiScreen(new GuiGrabber());
			GuiManager.init(new GuiTest());
		}
	}
	
	public static void mouse() {
		
	}
	
}
