package me.asi.client.util;

import org.lwjgl.Sys;

public class TimeUtil {

    public static double getTime() {
        return getTime(1);
    }

    public static double getTime(double mult) {
        return (Sys.getTime() * mult) / (double)Sys.getTimerResolution();
    }

}
