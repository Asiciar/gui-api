package me.asi.client.util;

public class MathUtil {

    public static double roundIncrement(double value, double increment) {
        return Math.round(value * (1.0 / increment)) / (1.0 / increment);
    }

}
