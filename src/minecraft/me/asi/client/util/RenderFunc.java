package me.asi.client.util;

import static org.lwjgl.opengl.GL11.*;

public class RenderFunc {

    private static int src, dst;

    public static void invert() {
        src = glGetInteger(GL_BLEND_SRC);
        dst = glGetInteger(GL_BLEND_DST);

        glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
    }

    public static void reset() {
        if (src != -1 && dst != -1) {
            glBlendFunc(src, dst);
            src = dst = -1;
        }
    }

}
