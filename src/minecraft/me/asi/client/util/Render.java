package me.asi.client.util;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CURRENT_COLOR;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LINE_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.GL_POINT_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_POLYGON;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SCISSOR_TEST;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4d;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glGetBoolean;
import static org.lwjgl.opengl.GL11.glGetFloat;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glPointSize;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glScissor;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;

import me.asi.client.gui.font.Font;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class Render {
	
	private static Minecraft mc = Minecraft.getMinecraft();
	
	private static int[] constants = {
			GL_ALPHA_TEST,
			GL_BLEND,
			GL_DEPTH_TEST,
			GL_LINE_SMOOTH,
			GL_POINT_SMOOTH,
			GL_TEXTURE_2D
	};
	private static boolean[] values = new boolean[constants.length];
	private static FloatBuffer color = BufferUtils.createFloatBuffer(16);
	
	private static void pre() {
		for (int i = 0; i < values.length; i++) {
			values[i] = glGetBoolean(constants[i]);
		}
		glGetFloat(GL_CURRENT_COLOR, color);
	
		glPushMatrix();
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glEnable(GL_LINE_SMOOTH);
		glEnable(GL_POINT_SMOOTH);
	}
	
	private static void post() {
		glPopMatrix();
		
		glColor4f(color.get(0), color.get(1), color.get(2), color.get(3));
		for(int i = values.length - 1; i >= 0; i--) {
			boolean val = values[i];
			if (val) {
				glEnable(constants[i]);
			} else {
				glDisable(constants[i]);
			}
		}
	}
	
	private static void color(int color) {
		double r = (color >> 16 & 255) / 255.0;
		double g = (color >> 8 & 255) / 255.0;
		double b = (color & 255) / 255.0;
		double a = (color >> 24 & 255) / 255.0;
		glColor4d(r, g, b, a);
	}
	
	public static void clipBegin(double x, double y, double w, double h) {
		glScissor((int) x, (int) (Display.getHeight() - (y + h)), (int) w, (int) h);
		glEnable(GL_SCISSOR_TEST);
	}
	
	public static void clipEnd() {
		glDisable(GL_SCISSOR_TEST);
	}
	
	public static void point(double x, double y, int color, double width) {
		width = Math.min(Math.max(0.01, width), 10);
		
		pre();
		
		color(color);
		
		glPointSize((float) width);
		glBegin(GL_POINTS);
		{
			glVertex2d(x, y);
		}
		glEnd();
		
		post();
	}
	
	public static void line(double x1, double y1, double x2, double y2, int color, double width) {
		width = Math.min(Math.max(0.01, width), 10);
		
		pre();
		
		color(color);
		
		glLineWidth((float) width);
		glBegin(GL_LINE_STRIP);
		{
			glVertex2d(x1, y1);
			glVertex2d(x2, y2);
		}
		glEnd();
		
		post();
	}
	
	public static void triangle(double x1, double y1, double x2, double y2, double x3, double y3, int color) {
		pre();
		
		color(color);
		
		glBegin(GL_POLYGON);
		{
			glVertex2d(x1, y1);
			glVertex2d(x3, y3);
			glVertex2d(x2, y2);
		}
		glEnd();
		
		post();
	}
	
	public static void rectangle(double x1, double y1, double x2, double y2, int color) {
		rectangle(x1, y1, x1, y2, x2, y2, x2, y1, color);
	}
	
	public static void rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color) {
		pre();
		
		color(color);
		
		glBegin(GL_QUADS);
		{
			glVertex2d(x1, y1);
			glVertex2d(x2, y2);
			glVertex2d(x3, y3);
			glVertex2d(x4, y4);
		}
		glEnd();
		
		post();
	}
	
	public static void rectangle(ResourceLocation texture, double x1, double y1, double x2, double y2) {
		rectangle(texture, x1, y1, x2, y2, 0xFFFFFFFF);
	}
	
	public static void rectangle(ResourceLocation texture, double x1, double y1, double x2, double y2, int color) {
		rectangle(texture, x1, y1, x1, y2, x2, y2, x2, y1, 0, 0, x2 - x1, y2 - y1, x2 - x1, y2 - y1, color);
	}
	
	public static void rectangle(ResourceLocation texture, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double u, double v, double us, double vs, double iw, double ih, int color) {
		pre();
		
		color(color);

		glEnable(GL_TEXTURE_2D);
		mc.getTextureManager().bindTexture(texture);

		double wScale = 1.0 / iw;
		double hScale = 1.0 / ih;

		glBegin(GL_QUADS);
		{
			glTexCoord2d(u * wScale, v * hScale);
			glVertex2d(x1, y1);
			glTexCoord2d(u * wScale, (v + vs) * hScale);
			glVertex2d(x2, y2);
			glTexCoord2d((u + us) * wScale, (v + vs) * hScale);
			glVertex2d(x3, y3);
			glTexCoord2d((u + us) * wScale, v * hScale);
			glVertex2d(x4, y4);
		}
		glEnd();

		post();
	}
	
	public static void string(Font font, String text, double x, double y, int color) {
		string(font, text, x, y, color, false);
	}
	
	public static void string(Font font, String text, double x, double y, int color, boolean shadow) {
		font.drawString(text, Math.round(x), Math.round(y), color, shadow);
	}
	
}
