package me.asi.client.util;

import static org.lwjgl.opengl.GL11.*;

public class StencilUtil {

    public static void pre() {
        glClearStencil(0);
        glClear(GL_STENCIL_BUFFER_BIT);
        glEnable(GL_STENCIL_TEST);
    }

    public static void hide() {
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        glColorMask(false, false, false, false);
    }

    public static void show() {
        glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        glColorMask(true, true, true, true);
    }

    public static void post() {
        glDisable(GL_STENCIL_TEST);
    }

}
