package me.asi.client.gui;

public interface Interactable {

	public void init();
	public void update();
	public void keyboard();
	public void mouse();
	public void interact(Component component);
	public void render();
	
}
