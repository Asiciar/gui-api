package me.asi.client.gui;

import me.asi.client.value.Value;
import org.lwjgl.input.Mouse;

public class Multichoice extends Component {

	public String text;
	public Value<String> val;

	public Multichoice(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}

	public Multichoice(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}

	public Multichoice setText(String text) {
		this.text = text;
		return this;
	}

	public Multichoice setVal(Value<String> val) {
		this.val = val;
		return this;
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0 && mouseOver()) {
			int index = -1;
			String cur = val.t[0];
			for(int i = 1; i < val.t.length; i++) {
				String s = val.t[i];
				if (s.equals(cur)) {
					index = i - 1;
					break;
				}
			}
			if (index != -1) {
				index++;
				index%= val.t.length - 1;
				val.t[0] = val.t[index + 1];
				parent.interact(this);
			}
		}
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}

}
