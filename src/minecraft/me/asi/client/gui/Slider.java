package me.asi.client.gui;

import me.asi.client.util.Input;
import me.asi.client.util.MathUtil;
import me.asi.client.value.Value;
import org.lwjgl.input.Mouse;

public class Slider extends Component {

	protected double[] padding = new double[4];
	public String text;
	public Value<Double> val;
	public boolean drag;

	public Slider(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}

	public Slider(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}

	public Slider setText(String text) {
		this.text = text;
		return this;
	}

	public Slider setVal(Value<Double> val) {
		this.val = val;
		return this;
	}

	public Slider setPadding(double top, double right, double bottom, double left) {
		this.padding = new double[] {top, right, bottom, left};
		return this;
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();

		if (drag) {
			double ww = w - (padding[1] + padding[3]);
			double mx = Input.mx() - (x + padding[3]);
			mx = Math.min(Math.max(0, mx), ww);
			double norm = (1.0 / ww) * mx;
			val.t[0] = val.t[1] + MathUtil.roundIncrement(norm * (val.t[2] - val.t[1]), val.t[3]);
			parent.interact(this);
		}
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0 && mouseOver()) {
			drag = true;
		} else if (button == 0) {
			drag = false;
		}
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}

}
