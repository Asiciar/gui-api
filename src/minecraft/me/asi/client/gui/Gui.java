package me.asi.client.gui;

public class Gui extends Component {

	public Gui() {
		super();
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}

	public void close() {}
}
