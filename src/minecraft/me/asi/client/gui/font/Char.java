package me.asi.client.gui.font;

public class Char {

	public int id;
	public int x, y, w, h, xoff, yoff, advance;
	
	public Char(int id, int x, int y, int w, int h, int xoff, int yoff, int advance) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.xoff = xoff;
		this.yoff = yoff;
		this.advance = advance;
	}
	
}
