package me.asi.client.gui.font;

public class FontManager {

	public static Font[][] fonts = new Font[Fonts.values().length][];
	
	static {
		for(Fonts f : Fonts.values()) {
			fonts[f.ordinal()] = FontLoader.loadAll(f);
		}
	}
	
	public static Font get(Fonts font, FontLoader.Size size) {
		try {
			return fonts[font.ordinal()][size.ordinal()];
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static enum Fonts {
		FONTAWESOME("fontawesome"),
		VERDANA("verdana");
		
		public String name;
		
		Fonts(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
	
}
