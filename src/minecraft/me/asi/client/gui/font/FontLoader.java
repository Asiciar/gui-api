package me.asi.client.gui.font;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import net.minecraft.util.ResourceLocation;

public class FontLoader {
	
	public static Font[] loadAll(FontManager.Fonts font) {
		Font[] fonts = new Font[Size.values().length];
		
		for(int i = 0; i < fonts.length; i++) {
			fonts[i] = load(font, Size.values()[i]);
		}
		
		return fonts;
	}

	public static Font load(FontManager.Fonts font, Size size) {
		ResourceLocation texture = new ResourceLocation("client/fonts/" + font + "/" + size.size + ".png");
		
		Map<Integer, Char> charMap = new HashMap<>();

		InputStream in = FontLoader.class.getResourceAsStream("/assets/minecraft/client/fonts/" + font.name + "/" + size.size + ".fnt");
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("char ")) {
					line = line.substring(5).replaceAll("\\s+", " ");

					int id = 0, x = 0, y = 0, w = 0, h = 0, xoff = 0, yoff = 0, advance = 0;

					String[] parts = line.split(" ");
					for (String part : parts) {
						String[] split = part.split("=");
						String key = split[0];
						int value;
						try {
							value = Integer.parseInt(split[1]);
						} catch (Exception e) {
							continue;
						}
						
						if (key.equalsIgnoreCase("id")) { 
							id = value;
						} else if (key.equalsIgnoreCase("x")) {
							x = value;
						} else if (key.equalsIgnoreCase("y")) {
							y = value;
						} else if (key.equalsIgnoreCase("width")) {
							w = value;
						} else if (key.equalsIgnoreCase("height")) {
							h = value;
						} else if (key.equalsIgnoreCase("xoffset")) {
							xoff = value;
						} else if (key.equalsIgnoreCase("yoffset")) {
							yoff= value;
						} else if (key.equalsIgnoreCase("xadvance")) {
							advance = value;
						}
					}
					charMap.put(id, new Char(id, x, y, w, h, xoff, yoff, advance));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			InputStream is = FontLoader.class.getResourceAsStream("/assets/minecraft/client/fonts/" + font + "/" + size.size + ".png");
			BufferedImage image = ImageIO.read(is);
			return new Font(texture, image.getWidth(), image.getHeight(), charMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static enum Size {
		XS(12),
		S(16),
		M(24),
		L(32),
		XL(48),
		XXL(64);
		
		public int size;
		
		Size(int size) {
			this.size = size;
		}
	}

}
