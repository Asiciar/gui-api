package me.asi.client.gui.font;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.asi.client.util.Render;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class Font {

	public static int[] colors = new int[32];

	static {
		for (int var5 = 0; var5 < 32; ++var5) {
			int var6 = (var5 >> 3 & 1) * 85;
			int var7 = (var5 >> 2 & 1) * 170 + var6;
			int var8 = (var5 >> 1 & 1) * 170 + var6;
			int var9 = (var5 & 1) * 170 + var6;

			if (var5 == 6) {
				var7 += 85;
			}

			if (var5 >= 16) {
				var7 /= 4;
				var8 /= 4;
				var9 /= 4;
			}

			colors[var5] = (var7 & 255) << 16 | (var8 & 255) << 8 | var9 & 255;
		}
	}

	public ResourceLocation texture;
	public int width, height;
	public Map<Integer, Char> charMap;

	public Font(ResourceLocation texture, int width, int height, Map<Integer, Char> charMap) {
		this.texture = texture;
		this.width = width;
		this.height = height;
		this.charMap = charMap;
	}

	public void drawString(String text, double x, double y, int color, boolean shadow) {
		y-= 2;
		
		double scale = 1;
		char[] chars = text.toCharArray();

		int alpha = (color >> 24) & 255;
		int red = (color >> 16) & 255;
		int green = (color >> 8) & 255;
		int blue = color & 255;

		boolean magic = false, bold = false, strike = false, underline = false, italic = false;
		int fcol = 15;

		String fcodes = "0123456789abcdefklmnor";
		
		for (int i = 0; i < chars.length; i++) {
			Char c = charMap.get((int) chars[i]);

			if (c == null) {
				continue;
			}

			if (chars[i] == '\u00a7' && i < chars.length - 1) {
				Char nc = charMap.get((int) chars[i + 1]);
				int ind = fcodes.indexOf((char) nc.id);
				if (ind >= 0 && ind <= 21) {
					switch (ind) {
					case 16:
						magic = true;
						break;
					case 17:
						bold = true;
						break;
					case 18:
						strike = true;
						break;
					case 19:
						underline = true;
						break;
					case 20:
						italic = true;
						break;
					case 21:
						bold = strike = underline = italic = false;
						fcol = 15;
						break;
					default:
						magic = bold = strike = underline = italic = false;
						fcol = ind;
						int cc = colors[fcol];
						red = (cc >> 16) & 255;
						green = (cc >> 8) & 255;
						blue = cc & 255;
						break;
					}
					i++;
					continue;
				}
			}

			if (magic) {
				Random random = new Random();

				List<Char> list = new ArrayList<>();
				list.addAll(charMap.values());
				int m;
				do {
					m = random.nextInt(charMap.size());
				} while (list.get(m).w != c.w);

				c = list.get(m);
			}

			if (strike) {
				Render.rectangle(x, y + Math.round(height() / 2.0), x + c.advance, y + Math.round(height() / 2.0) + 1,
						color);
			}

			if (underline) {
				Render.rectangle(x, y + height(), x + c.advance, y + height() + 1, color);
			}

			for (int j = (shadow ? 1 : 0); j >= 0; j--) {
				for (int k = 0; k < (bold ? 2 : 1); k++) {
					GlStateManager.pushMatrix();
					int ac = red << 16 | green << 8 | blue | alpha << 24;
					if (j == 1) {
						GlStateManager.translate(1, 1, 0);
						ac = colors[fcol + 16];
					}
					if (k == 1) {
						GlStateManager.translate(1, 0, 0);
					}

					double xx1 = x + (c.xoff * scale);
					double yy1 = y + (c.yoff * scale);
					double xx2 = xx1 + (c.w * scale);
					double yy2 = yy1 + (c.h * scale);
					double it = italic ? 3 : 0;

					Render.rectangle(texture, xx1 + it, yy1, xx1, yy2, xx2, yy2, xx2 + it, yy1, c.x, c.y, c.w, c.h, width, height, ac);
					GlStateManager.popMatrix();
				}
			}

			x += (c.advance - 4) * scale;
		}
	}

	public double width(String text) {
		double scale = 1;
		double w = 1;
		char[] chars = text.toCharArray();
		boolean magic = false;
		String fcodes = "0123456789abcdefklmnor";

		for (int i = 0; i < chars.length; i++) {
			Char c = charMap.get((int) chars[i]);
			
			if (c == null) {
				continue;
			}
			
			if (chars[i] == '\u00a7' && i < chars.length - 1) {
				Char nc = charMap.get((int) chars[i + 1]);
				int ind = fcodes.indexOf((char) nc.id);
				if (ind >= 0 && ind <= 21) {
					switch (ind) {
					case 16:
						magic = true;
						break;
					default:
						magic = false;
						break;
					}
					i++;
					continue;
				}
			}

			if (magic) {
				Random random = new Random();

				List<Char> list = new ArrayList<>();
				list.addAll(charMap.values());
				int m;
				do {
					m = random.nextInt(charMap.size());
				} while (list.get(m).w != c.w);

				c = list.get(m);
			}
			
			w += (c.advance - 4) * scale;
		}
		return w;
	}
	
	public double height(String text) {
		double height = 0;
		char[] chars = text.toCharArray();
		boolean magic = false;
		String fcodes = "0123456789abcdefklmnor";
		
		for (int i = 0; i < chars.length; i++) {
			Char c = charMap.get((int) chars[i]);
			
			if (c == null) {
				continue;
			}
			
			if (chars[i] == '\u00a7' && i < chars.length - 1) {
				Char nc = charMap.get((int) chars[i + 1]);
				int ind = fcodes.indexOf((char) nc.id);
				if (ind >= 0 && ind <= 21) {
					switch (ind) {
					case 16:
						magic = true;
						break;
					default:
						magic = false;
						break;
					}
					i++;
					continue;
				}
			}

			if (magic) {
				Random random = new Random();

				List<Char> list = new ArrayList<>();
				list.addAll(charMap.values());
				int m;
				do {
					m = random.nextInt(charMap.size());
				} while (list.get(m).w != c.w);

				c = list.get(m);
			}
			
			height = Math.max(c.h, height);
		}
		return height;
	}
	
	public double height() {
		return height("AaBbCcZzYyXx");
	}
}
