package me.asi.client.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;

public class GuiManager {

	private static Minecraft mc = Minecraft.getMinecraft();
	
	public static Gui gui;
	
	public static void init(Gui gui2) {
		gui = gui2;
	}
	
	public static void update() {
		if (gui != null) {
			gui.update();
			
			while (Keyboard.next()) {
				keyboard();
			}
			
			while (Mouse.next()) {
				mouse();
			}
		}
	}
	
	public static void keyboard() {
		if (Keyboard.getEventKeyState() && Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
			mc.displayGuiScreen(null);
			gui.close();
			gui = null;
			return;
		}
		if (gui != null) {
			gui.keyboard();
		}
	}
	
	public static void mouse() {
		if (gui != null) {
			gui.mouse();
		}
	}
	
	public static void render() {
		ScaledResolution resolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
		GlStateManager.pushMatrix();
		
		double scale = resolution.getScaleFactor() / Math.pow(resolution.getScaleFactor(), 2);
		GlStateManager.scale(scale, scale, scale);
		
		if (gui != null) {
			gui.render();
		}
		
		GlStateManager.popMatrix();
	}
	
}
