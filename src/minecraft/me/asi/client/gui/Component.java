package me.asi.client.gui;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.collect.Lists;

import me.asi.client.util.Input;

public class Component implements Interactable {

	public List<Component> components = new CopyOnWriteArrayList<>();
	
	public Component parent;
	public int id;
	public double x, y, w, h, offsetX, offsetY;
	public boolean hover;
	
	public Component() {
		this(null, -1);
	}
	
	public Component(Component parent, int id) {
		this(parent, id, 0, 0, 0, 0);
	}
	
	public Component(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, parent != null ? x - parent.x : 0, parent != null ? y - parent.y : 0);
	}
	
	public Component(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		this.parent = parent;
		this.id = id;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}

	@Override
	public void init() {

	}

	@Override
	public void update() {
		hover = mouseOver();

		components.forEach(Component::update);
	}

	@Override
	public void keyboard() {
		components.forEach(Component::keyboard);
	}

	@Override
	public void mouse() {
		components.forEach(Component::mouse);
	}
	
	public boolean mouseOver() {
		return mouseOver(x, y, w, h);
	}
	
	public boolean mouseOver(double x, double y, double w, double h) {
		return Input.mx() >= x && Input.mx() < x + w && Input.my() > y && Input.my() <= y + h;
	}

	@Override
	public void interact(Component component) {

	}

	@Override
	public void render() {
		components.forEach(Component::render);
	}

	public Component getFromID(int id) {
		for(Component component : components) {
			if (component.id == id) {
				return component;
			}
		}
		return null;
	}
	
}
