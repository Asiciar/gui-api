package me.asi.client.gui;

import org.lwjgl.input.Mouse;

public class Button extends Component {

	public Button(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}
	
	public Button(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}

	@Override
	public void init() {

	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0 && mouseOver()) {
			parent.interact(this);
		}
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}
	
}
