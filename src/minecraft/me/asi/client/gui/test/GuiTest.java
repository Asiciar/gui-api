package me.asi.client.gui.test;

import me.asi.client.gui.Component;
import me.asi.client.gui.Gui;
import org.lwjgl.input.Keyboard;

public class GuiTest extends Gui {

	public GuiTest() {
		super();
		Keyboard.enableRepeatEvents(true);
		init();
	}

	@Override
	public void init() {
		components.add(new FrameTest(this, 0, 32, 32, 200, 400).setTitle("Frame"));
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
	}

	@Override
	public void interact(Component component) {
		switch (component.id) {
			default:
				break;
		}
	}

	@Override
	public void render() {
		super.render();
	}
}
