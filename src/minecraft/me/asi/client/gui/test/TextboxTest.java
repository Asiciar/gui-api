package me.asi.client.gui.test;

import me.asi.client.gui.Component;
import me.asi.client.gui.Textbox;
import me.asi.client.util.*;

public class TextboxTest extends Textbox {

	public TextboxTest(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}

	public TextboxTest(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}
	
	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
	}

	public boolean mouseOver(double x, double y, double w, double h) {
		return super.mouseOver(x, y, w, h) && parent.mouseOver();
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
		Render.rectangle(x, y, x + w, y + h, hover || focus ? 0x80FF0000 : 0x80000000);

		Render.string(font, placeholder, x + 4, y + (h / 2.0) - (font.height() / 2.0), 0xFFFFFFFF);

		StencilUtil.pre();

		StencilUtil.hide();

		Render.rectangle(x, y, x + padding[3], y + h, 0xFFFFFFFF);
		Render.rectangle(x + padding[3], y, x + w - padding[1], y + padding[0], 0xFFFFFFFF);
		Render.rectangle(x + padding[3], y + h - padding[2], x + w - padding[1], y + h, 0xFFFFFFFF);
		Render.rectangle(x + w - padding[1], y, x + w, y + h, 0xFFFFFFFF);

		StencilUtil.show();

		Render.string(font, text, x + padding[3] + scroll, y + (h / 2.0) - (font.height() / 2.0), 0xFFFFFFFF);

		if (focus) {
			if (selectStart != selectEnd) {
				double offset1 = Math.min(font.width(text.substring(0, selectStart)), font.width(text.substring(0, selectEnd)));
				double offset2 = Math.max(font.width(text.substring(0, selectStart)), font.width(text.substring(0, selectEnd)));

				Render.rectangle(x + padding[3] + scroll + offset1, y + padding[0], x + padding[3] + scroll + offset2, y + h - padding[2], 0x80000000);
			}

			double offset = font.width(text.substring(0, index));
			int time = (int) TimeUtil.getTime(2);
			Render.line(x + padding[3] + scroll + offset, y + padding[0], x + padding[3] + scroll + offset, y + h - padding[2], time % 2 == 0 ? 0xFFFFFFFF : 0x80FFFFFF, 1);
		}

		StencilUtil.post();
	}
}
