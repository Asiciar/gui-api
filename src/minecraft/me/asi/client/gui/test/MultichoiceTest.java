package me.asi.client.gui.test;

import me.asi.client.gui.Component;
import me.asi.client.gui.Multichoice;
import me.asi.client.gui.font.Font;
import me.asi.client.gui.font.FontLoader;
import me.asi.client.gui.font.FontManager;
import me.asi.client.util.Render;

public class MultichoiceTest extends Multichoice {

	public MultichoiceTest(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}

	public MultichoiceTest(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
		Render.rectangle(x, y, x + w, y + h, hover ? 0x80FF0000 : 0x80000000);
		Font f = FontManager.get(FontManager.Fonts.VERDANA, FontLoader.Size.S);

		String display = text + ": " + val.t[0];
		Render.string(f, display, x + 4, y + (h / 2.0) - (f.height() / 2.0), 0xFFFFFFFF);
	}

}
