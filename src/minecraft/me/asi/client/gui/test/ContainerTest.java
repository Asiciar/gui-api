package me.asi.client.gui.test;

import me.asi.client.gui.Component;
import me.asi.client.gui.Container;
import me.asi.client.gui.font.FontLoader;
import me.asi.client.gui.font.FontManager;
import me.asi.client.value.Value;

public class ContainerTest extends Container {

	public ContainerTest(Component parent, int id, double x, double y, double w, double h, boolean offsets, double scrollIncrement) {
		this(parent, id, x, y, w, h, offsets, true, scrollIncrement);
	}

	public ContainerTest(Component parent, int id, double x, double y, double w, double h, boolean offsets, boolean clip, double scrollIncrement) {
		super(parent, id, x, y, w, h, offsets, clip, scrollIncrement);
		init();
	}
	
	@Override
	public void init() {
		int j = 0;
		for(int i = 0; i < 1; i++) {
			components.add(new ButtonTest(this, j, x, y + (j++ * 32), w, 32).setText("Button"));
			components.add(new MultichoiceTest(this, j, x, y + (j++ * 32), w, 32).setText("Choice").setVal(new Value<>("First", "First", "Second", "Third", "Fourth")));
			components.add(new SliderTest(this, j, x, y + (j++ * 32), w, 32).setText("Value").setPadding(4, 0, 4, 0).setVal(new Value<>(5.0, 5.0, 10.0, 0.1)));
			components.add(new TextboxTest(this, j, x, y + (j++ * 32), w, 32).setPlaceholder("Text: ").setPadding(4, 4, 4, FontManager.get(FontManager.Fonts.VERDANA, FontLoader.Size.S).width("Text: ")));
			components.add(new ToggleTest(this, j, x, y + (j++ * 32), w, 32).setText("Toggle").setVal(new Value<>(true)));
		}
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
	}

	@Override
	public void interact(Component component) {
		super.interact(component);
	}

	@Override
	public void render() {
		super.render();
	}
	
}
