package me.asi.client.gui.test;

import me.asi.client.gui.font.Font;
import org.lwjgl.input.Mouse;

import me.asi.client.gui.Component;
import me.asi.client.gui.Container;
import me.asi.client.gui.font.FontLoader;
import me.asi.client.gui.font.FontManager;
import me.asi.client.util.Input;
import me.asi.client.util.Render;

public class FrameTest extends Container {

	public String title;
	public boolean drag;
	public double dragX, dragY;
	
	public FrameTest(Component parent, int id, double x, double y, double w, double h) {
		super(parent, id, x, y, w, h, true, false, 0);
		init();
	}

	public FrameTest setTitle(String title) {
		this.title = title;
		return this;
	}
	
	@Override
	public void init() {
		components.add(new ContainerTest(this, -1, x, y + 32, w - 8, 64, true, 32).setMaxHeight(300));
	}

	@Override
	public void update() {
		if (drag) {
			x = Input.mx() + dragX;
			y = Input.my() + dragY;
		}
		super.update();
		h = components.get(0).h + 32;
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0 && mouseOver(x, y, w, 32)) {
			drag = true;
			dragX = x - Input.mx();
			dragY = y - Input.my();
		} else if (button == 0) {
			drag = false;
		}
	}

	@Override
	public void interact(Component component) {
		super.interact(component);
	}

	@Override
	public void render() {
		Font font = FontManager.get(FontManager.Fonts.VERDANA, FontLoader.Size.S);

		Render.rectangle(x, y, x + w, y + h, 0x80000000);
		Render.string(font, title, x + (w / 2.0) - (font.width(title) / 2.0), y + 16 - (font.height() / 2.0), 0xFFFFFFFF);

		Container container = (Container) components.get(0);
		double componentHeight = 0;
		for(Component component : container.components) {
			componentHeight+= component.h;
		}

		if (componentHeight > container.h) {
			Render.rectangle(container.x + container.w + 1, container.y + (container.scrollNormal * container.h), x + w - 1, container.y + (container.scrollNormal * container.h) + container.scrollHeight, 0xFFFFFFFF);
		}

		super.render();
	}
	
}
