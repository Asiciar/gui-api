package me.asi.client.gui;

import me.asi.client.util.*;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Keyboard;

import me.asi.client.gui.font.Font;
import me.asi.client.gui.font.FontLoader;
import me.asi.client.gui.font.FontManager;
import org.lwjgl.input.Mouse;

public class Textbox extends Component {

	protected double[] padding = new double[4];
	public boolean focus, drag, shift, control;
	public String placeholder = "", text = "";
	public int index, selectStart, selectEnd, limit = 10000;
	public double scroll;
	public Font font = FontManager.get(FontManager.Fonts.VERDANA, FontLoader.Size.S);
	
	public Textbox(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}
	
	public Textbox(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}
	
	public Textbox setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
		return this;
	}
	
	public Textbox setText(String text) {
		this.text = text;
		setIndex(text.length());
		return this;
	}

	public Textbox setCharLimit(int limit) {
		this.limit = limit;
		return this;
	}

	public Textbox setFocus(boolean focus) {
		this.focus = focus;
		return this;
	}

	public Textbox setFont(Font font) {
		this.font = font;
		return this;
	}

	public Textbox setPadding(double top, double right, double bottom, double left) {
		this.padding = new double[] {top, right, bottom, left};
		return this;
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
		
		hover = mouseOver();

		if (drag) {
			index = getMouseIndex((x + padding[3]) + scroll, text);
			selectEnd = index;
		}

		if (!focus) {
			setIndex(0);
		}

		double pos = getXFromIndex(index, text) + scroll;
		double ww = (w - (padding[1] + padding[3]));
		if (pos > ww) {
			scroll+= (ww - pos);
		}
		if (pos < 0) {
			scroll-= pos;
		}

		if (scroll > -1) {
			scroll = -1;
		}

		double farX = getXFromIndex(text.length(), text);
		if (scroll < -1 && farX < ww - scroll) {
			scroll+= (ww - scroll) - farX;
		}
	}

	@Override
	public void keyboard() {
		super.keyboard();
		
		boolean state = Keyboard.getEventKeyState();
		int key = Keyboard.getEventKey();
		char character = Keyboard.getEventCharacter();
		
		if (focus) {
			switch (key) {
				case Keyboard.KEY_LSHIFT:
				case Keyboard.KEY_RSHIFT:
					shift = state;
					break;
				case Keyboard.KEY_LCONTROL:
				case Keyboard.KEY_RCONTROL:
					control = state;
					break;
			}
			if (state) {
				if (isAllowedCharacter(character)) {
					if (selectStart != selectEnd) {
						text = StringUtil.replace(text, String.valueOf(character), selectStart, selectEnd);
						setIndex(Math.min(selectStart, selectEnd) + 1);
					} else {
						text = StringUtil.insert(text, String.valueOf(character), index);
						setIndex(index + 1);
					}
				}

				int indexTo;
				switch (key) {
					case Keyboard.KEY_ESCAPE:
						focus = false;
						break;
					case Keyboard.KEY_RETURN:
						parent.interact(this);
						focus = false;
						break;
					case Keyboard.KEY_BACK:
						if (text.length() > 0 && index > 0 || selectStart != selectEnd) {
							if (selectStart != selectEnd) {
								text = StringUtil.replace(text, "", selectStart, selectEnd);
								setIndex(Math.min(selectStart, selectEnd));
							} else {
								if (control) {
									int to = getWordSelection(index, text)[0];
									text = StringUtil.replace(text, "", to, index);
									setIndex(to);
								} else {
									text = StringUtil.remove(text, index - 1, index);
									setIndex(index - 1);
								}
							}
						}
						break;
					case Keyboard.KEY_DELETE:
						if (text.length() > 0 && index < text.length() || selectStart != selectEnd) {
							if (selectStart != selectEnd) {
								text = StringUtil.replace(text, "", selectStart, selectEnd);
								setIndex(Math.min(selectStart, selectEnd));
							} else {
								if (control) {
									int to = getWordSelection(index, text)[1];
									text = StringUtil.replace(text, "", index, to);
								} else {
									text = StringUtil.remove(text, index, index + 1);
								}
							}
						}
						break;
					case Keyboard.KEY_LEFT:
						indexTo = index - 1;
						if (control) {
							indexTo = getWordSelection(index, text)[0];
						}
						if (shift) {
							index = indexTo;
							selectEnd = Math.max(index, 0);
						} else {
							setIndex(indexTo);
						}
						break;
					case Keyboard.KEY_RIGHT:
						indexTo = index + 1;
						if (control) {
							indexTo = getWordSelection(index, text)[1];
						}
						if (shift) {
							index = indexTo;
							selectEnd = Math.max(index, 0);
						} else {
							setIndex(indexTo);
						}
						break;
					case Keyboard.KEY_A:
						if (control) {
							selectStart = 0;
							selectEnd = text.length();
							index = selectEnd;
						}
						break;
					case Keyboard.KEY_C:
						if (selectStart != selectEnd) {
							GuiScreen.setClipboardString(text.substring(Math.min(selectStart, selectEnd), Math.max(selectStart, selectEnd)));
						}
						break;
					case Keyboard.KEY_X:
						if (selectStart != selectEnd) {
							GuiScreen.setClipboardString(text.substring(Math.min(selectStart, selectEnd), Math.max(selectStart, selectEnd)));
							text = StringUtil.replace(text, "", selectStart, selectEnd);
							setIndex(Math.min(selectStart, selectEnd));
						}
						break;
					case Keyboard.KEY_V:
						if (selectStart != selectEnd) {
							text = StringUtil.replace(text, GuiScreen.getClipboardString(), selectStart, selectEnd);
							setIndex(Math.min(selectStart, selectEnd) + GuiScreen.getClipboardString().length());
						} else {
							text = StringUtil.insert(text, GuiScreen.getClipboardString(), index);
							setIndex(index + GuiScreen.getClipboardString().length());
						}
						break;
					case Keyboard.KEY_HOME:
						if (shift) {
							selectStart = index;
							index = 0;
							selectEnd = index;
						} else {
							setIndex(0);
						}
						break;
					case Keyboard.KEY_END:
						if (shift) {
							selectStart = index;
							index = text.length();
							selectEnd = index;
						} else {
							setIndex(text.length());
						}
						break;
				}

				index = Math.min(Math.max(0, index), text.length());
			}
		}

		if (text.length() > limit) {
			text = text.substring(0, limit);
		}
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0) {
			focus = mouseOver();

			if (focus) {

				drag = true;

				int nearIndex = getMouseIndex(x + padding[3] + scroll, text);
				if (nearIndex >= 0 && nearIndex <= text.length()) {
					setIndex(nearIndex);
				} else {
					setIndex(text.length());
				}
			}
		} else if (button == 0) {
			drag = false;
		}
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}

	protected boolean isAllowedCharacter(char character) {
		return character != 167 && character >= 32 && character != 127;
	}

	protected void setIndex(int index) {
		this.index = index;
		this.selectStart = index;
		this.selectEnd = index;
	}

	protected int getMouseIndex(double startX, String text) {
		int nearIndex = 0;
		if (Input.mx() < startX) {
			return 0;
		}
		for(int i = 0; i < text.length(); i++) {
			double xx = startX + font.width(text.substring(0, i));
			if (xx >= Input.mx()) {
				if (i == 0) {
					nearIndex = i;
					break;
				}
				double xxPrev = startX + getXFromIndex(i - 1, text);
				if (Math.abs(xx - Input.mx()) < Math.abs(xxPrev - Input.mx())) {
					nearIndex = i;
				} else {
					nearIndex = i - 1;
				}
				break;
			} else {
				if (i == text.length() - 1) {
					double xxNext = startX + font.width(text);
					if (Math.abs(xx - Input.mx()) > Math.abs(xxNext - Input.mx())) {
						nearIndex = i + 1;
					} else {
						nearIndex = i;
					}
				}
			}
		}
		return nearIndex;
	}

	protected double getXFromIndex(int index, String text) {
		return font.width(text.substring(0, index));
	}

	protected int[] getWordSelection(double xx, String text) {
		int start = 0;
		int end = text.length();

		for(int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (c == ' ') {
				if (i < xx) {
					start = i;
				}
				if (i >= xx) {
					end = i + 1;
					break;
				}
			}
		}

		return new int[] {start, end};
	}
}
