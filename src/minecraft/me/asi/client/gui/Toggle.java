package me.asi.client.gui;

import me.asi.client.value.Value;
import org.lwjgl.input.Mouse;

public class Toggle extends Component {

	public String text;
	public Value<Boolean> val;

	public Toggle(Component parent, int id, double x, double y, double w, double h) {
		this(parent, id, x, y, w, h, x - parent.x, y - parent.y);
	}

	public Toggle(Component parent, int id, double x, double y, double w, double h, double offsetX, double offsetY) {
		super(parent, id, x, y, w, h, offsetX, offsetY);
	}

	public Toggle setText(String text) {
		this.text = text;
		return this;
	}

	public Toggle setVal(Value<Boolean> val) {
		this.val = val;
		return this;
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();

		boolean state = Mouse.getEventButtonState();
		int button = Mouse.getEventButton();

		if (state && button == 0 && mouseOver()) {
			val.t[0] = !val.t[0];
			parent.interact(this);
		}
	}

	@Override
	public void interact(Component component) {
	}

	@Override
	public void render() {
		super.render();
	}

}
