package me.asi.client.gui;

import org.lwjgl.input.Mouse;

import me.asi.client.util.Render;

public class Container extends Component {

	public double scroll, scrollIncrement, scrollHeight, scrollNormal, maxHeight;
	public boolean clip, offsets;
	
	public Container(Component parent, int id, double x, double y, double w, double h, boolean offsets, double scrollIncrement) {
		this(parent, id, x, y, w, h, offsets, true, scrollIncrement);
	}
	
	public Container(Component parent, int id, double x, double y, double w, double h, boolean offsets, boolean clip, double scrollIncrement) {
		super(parent, id, x, y, w, h);
		
		this.offsets = offsets;
		this.clip = clip;
		this.scrollIncrement = scrollIncrement;
		this.maxHeight = h;
	}

	public Container setMaxHeight(double maxHeight) {
		this.maxHeight = maxHeight;
		return this;
	}

	@Override
	public void update() {
		double i = 0;
		for(Component component : components) {
			if (offsets) {
				component.x = x + component.offsetX;
				component.y = y - scroll + component.offsetY;
			} else {
				component.x = x;
				component.y = y - scroll + i;
			}
			i+= component.h;
		}

		h = Math.min(i, maxHeight);

		scrollHeight = h / (i / h);
		scrollNormal = (1.0 / i) * scroll;

		super.update();
	}

	@Override
	public void keyboard() {
		super.keyboard();
	}

	@Override
	public void mouse() {
		super.mouse();
		if (mouseOver()) {
			scroll-= Mouse.getEventDWheel() / 120.0 * scrollIncrement;
			
			double max = 0;
			for(Component component : components) {
				max+= component.h;
			}
			
			scroll = Math.min(Math.max(0, scroll), Math.max(0, max - h));
		}
	}

	@Override
	public void interact(Component component) {
		parent.interact(component);
	}

	@Override
	public void render() {
		if (clip) {
			Render.clipBegin(x, y, w, h);
		}

		super.render();
		
		if (clip) {
			Render.clipEnd();
		}
	}
	
}
